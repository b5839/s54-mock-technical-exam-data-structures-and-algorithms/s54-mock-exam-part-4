let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.
 function print(){
 	return collection;
 }
 function enqueue(person){
 	 collection[collection.length]=person;
 	 return collection;
 }
function dequeue(){
		let newQueue=[];
	for(let i=1;i<collection.length;i++){
			newQueue[newQueue.length] = collection[i];
	}
	collection=newQueue;
	return collection;
}
function front(){

	return collection[0];

}
function size(){

	return collection.length;
}
function isEmpty(){

	return collection.length === 0;
}




module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};